[require]
shader model >= 4.0


[texture 0]
size (1, 1)
0.0 0.0 0.0 99.0

[texture 1]
size (1, 1)
1.0 1.0 1.0 99.0

[texture 2]
size (1, 1)
2.0 2.0 2.0 99.0

[texture 3]
size (1, 1)
3.0 3.0 3.0 99.0

[texture 4]
size (1, 1)
4.0 4.0 4.0 99.0


% If a single component in a texture array is used, all registers are reserved.
[pixel shader]
Texture2D partially_used[2][2];
Texture2D tex;

float4 main() : sv_target
{
    return 10 * tex.Load(int3(0, 0, 0)) + partially_used[0][1].Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (41.0, 41.0, 41.0, 1089.0)


% If no component in a texture array is used, and it doesn't have a register reservation, no
% register is reserved.
[pixel shader]
Texture2D unused[4];
Texture2D tex;

float4 main() : sv_target
{
    return tex.Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (0.0, 0.0, 0.0, 99.0)


% Register reservations force to reserve all the resource registers. Even if unused.
[pixel shader]
Texture2D unused : register(t0);
Texture2D tex;

float4 main() : sv_target
{
    return tex.Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (1.0, 1.0, 1.0, 99.0)


% Register reservation with incorrect register type.
[pixel shader]
Texture2D unused : register(s0);
Texture2D tex;

float4 main() : sv_target
{
    return tex.Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (0.0, 0.0, 0.0, 99.0)


% Register reservation with incorrect register type.
[pixel shader]
sampler2D unused : register(t0);
Texture2D tex;

float4 main() : sv_target
{
    return tex.Load(int3(0, 0, 0));
}


[test]
draw quad
probe all rgba (0.0, 0.0, 0.0, 99.0)

[pixel shader]
Texture2D unused[2][2] : register(t0);
Texture2D tex;

float4 main() : sv_target
{
    return tex.Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (4.0, 4.0, 4.0, 99.0)


% Overlapping reservations, both overlapping objects are unused.
[pixel shader]
Texture2D tex1 : register(t0);
Texture2D tex2 : register(t0);
Texture2D tex3;

float4 main() : sv_target
{
    return tex3.Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (1.0, 1.0, 1.0, 99.0)


% Overlapping reservations
[pixel shader]
Texture2D tex1 : register(t2);
Texture2D tex2 : register(t2);

float4 main() : sv_target
{
    return tex1.Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (2.0, 2.0, 2.0, 99.0)


[pixel shader]
Texture2D tex1 : register(t2);
Texture2D tex2 : register(t2);

float4 main() : sv_target
{
    return tex2.Load(int3(0, 0, 0));
}

[test]
draw quad
probe all rgba (2.0, 2.0, 2.0, 99.0)
